# Automated k6 load testing with Gitlab CI/CD

This is an example repo for how to setup k6 with Gitlab CI/CD to add load testing into an automation flow.

The full guide describing how to use this repository is located at: https://k6.io/blog/integrating-load-testing-with-gitlab
